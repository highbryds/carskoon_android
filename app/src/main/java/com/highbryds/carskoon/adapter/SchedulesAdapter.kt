package com.highbryds.carskoon.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.models.VehicleSchedule
import kotlinx.android.synthetic.main.vehicle_schedule.view.*


class SchedulesAdapter(private val context: Context) :
    BaseRecyclerViewAdapter<VehicleSchedule>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.vehicle_schedule, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myHolder = holder as? MyViewHolder
        myHolder?.setUpView(getItem(position))
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var model: VehicleSchedule? = null

        init {
            view.setOnClickListener(this)
        }

        private val regNumber: TextView = view.regNumber
        private val wash_date: TextView = view.wash_date
        private val preferred_time: TextView = view.preferred_time


        fun setUpView(schdules: VehicleSchedule?) {
            model = schdules

            regNumber.text = schdules?.RegNumber
            wash_date.text = schdules?.WashingDate
            preferred_time.text = schdules?.WashingTime


//            txtOrderNo.text ="Order# "+ paymentHistory?.orderNo
//            txtPackageNames.text ="Package Name: "+ paymentHistory?.packageName
//            txtModel.text ="Registered Car: "+
//                paymentHistory?.vehicleProfile?.get(0)?.model + "(" + paymentHistory?.vehicleProfile?.get(0)?.regNumber + ")"
//            txtRequestedType.text ="Requested Type: "+ paymentHistory?.requestType
//            txtStatus.text = paymentHistory?.status
//            txtDate.text ="Date: "+ paymentHistory?.requestOn


        }


        override fun onClick(v: View?) {


            model?.let { itemClickListener.onItemClick(it, absoluteAdapterPosition, v) }
        }
    }
}