package com.highbryds.carskoon.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.highbryds.carskoon.Fragments.AddVehicleFragment
import com.highbryds.carskoon.Fragments.SchedulesFragment
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.models.PaymentHistory
import kotlinx.android.synthetic.main.item_payment_history.view.*


class PaymentHistoryAdapter(private val context: Context) :
    BaseRecyclerViewAdapter<PaymentHistory>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_payment_history, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myHolder = holder as? MyViewHolder
        myHolder?.setUpView(getItem(position))
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var model: PaymentHistory? = null

        init {
            view.setOnClickListener(this)
        }

        private val txtOrderNo: TextView = view.txtOrderNo
        private val txtDate: TextView = view.txtDate
        private val txtPackageNames: TextView = view.txtPackageNames
        private val txtModel: TextView = view.txtModel
        private val txtRequestedType: TextView = view.txtRequestedType
        private val txtStatus: TextView = view.txtStatus
        private val payment_card: CardView = view.payment_card


        fun setUpView(paymentHistory: PaymentHistory?) {
            model = paymentHistory

            txtOrderNo.text ="Order# "+ paymentHistory?.orderNo
            txtPackageNames.text ="Package Name: "+ paymentHistory?.packageName
            txtModel.text ="Registered Car: "+
                paymentHistory?.vehicleProfile?.get(0)?.model + "(" + paymentHistory?.vehicleProfile?.get(0)?.regNumber + ")"
            txtRequestedType.text ="Requested Type: "+ paymentHistory?.requestType
            txtStatus.text = paymentHistory?.status
            txtDate.text ="Date: "+ paymentHistory?.requestOn


        }


        override fun onClick(v: View?) {



            model?.let { itemClickListener.onItemClick(it, absoluteAdapterPosition, v) }
        }
    }
}