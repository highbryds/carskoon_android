package com.highbryds.carskoon.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.models.Packages
import kotlinx.android.synthetic.main.item_my_packages.view.*
import kotlinx.android.synthetic.main.item_my_vehicle.view.txtRegNo


class MyPackagesAdapter(private val context: Context) : BaseRecyclerViewAdapter<Packages>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_my_packages, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myHolder = holder as? MyViewHolder
        myHolder?.setUpView(getItem(position))
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var model: Packages? = null

//        init {
//            view.setOnClickListener(this)
//        }

        private val txtPackageName: TextView = view.txtPackageName
        private val txtRegNo: TextView = view.txtRegNo
        private val txtStartDate: TextView = view.txtStartDate
        private val txtEndDate: TextView = view.txtEndDate
        private val txtPaymentStatus: TextView = view.txtPaymentStatus


        fun setUpView(myPackages: Packages?) {

            txtPackageName.text = myPackages?.Package
            txtRegNo.text = "Register No: " + myPackages?.RegNumber
            txtStartDate.text = "Start Date: " + myPackages?.StartDate
            txtEndDate.text = "End Date: " + myPackages?.EndDate

            txtPaymentStatus.text = myPackages?.PaymentStatus


        }

        override fun onClick(v: View?) {
            model?.let { itemClickListener.onItemClick(it, absoluteAdapterPosition, v) }
        }
    }
}