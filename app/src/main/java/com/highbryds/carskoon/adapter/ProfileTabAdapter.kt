package com.highbryds.carskoon.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.highbryds.carskoon.Fragments.MyPackagesFragment
import com.highbryds.carskoon.Fragments.MyVehicleFragment
import com.highbryds.carskoon.Fragments.ProfileFragment
import com.highbryds.carskoon.Helpers.GeneralHelper


class ProfileTabAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {


    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                // checkVariable
                //    if (checkVariable == 1) {

                //   GeneralHelper.loadFragment()

                return ProfileFragment.newInstance()
//                } else {
//
//                    return ProfileFragment.newInstance()
//
//                }


            }

            1 -> return MyVehicleFragment.newInstance()
            2 -> return MyPackagesFragment.newInstance()
        }
        return MyVehicleFragment.newInstance()
    }

    override fun getItemCount(): Int {
        return 3
    }


}
