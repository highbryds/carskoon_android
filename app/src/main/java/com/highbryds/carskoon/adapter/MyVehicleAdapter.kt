package com.highbryds.carskoon.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.models.MyVehicleList
import kotlinx.android.synthetic.main.item_my_vehicle.view.*


class MyVehicleAdapter(ClickEvent: ClickEvent) : BaseRecyclerViewAdapter<MyVehicleList>() {
    var var_ClickEvent: ClickEvent

    init {
        this.var_ClickEvent = ClickEvent
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_my_vehicle, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myHolder = holder as? MyViewHolder
        myHolder?.setUpView(getItem(position))



        myHolder?.delete_car?.setOnClickListener {

            getItem(position)?.let { it1 -> var_ClickEvent.onDeleteClicked(it1.V_Id, position) }


        }


    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var model: MyVehicleList? = null

        private val imgCar: ImageView = view.imgCar
        private val txtCarName: TextView = view.txtCarName
        private val txtRegNo: TextView = view.txtRegNo
        private val txtColor: TextView = view.txtColor
        internal var delete_car: ImageView = view.delete_car


        fun setUpView(myVehicles: MyVehicleList?) {


            txtCarName.text = myVehicles?.Model
            txtRegNo.text = myVehicles?.RegNumber
            txtColor.text = myVehicles?.Color
            if (myVehicles?.Model?.contains("toyota", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.toyota)

            } else if (myVehicles?.Model?.contains("honda", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.honda)

            } else if (myVehicles?.Model?.contains("suzuki", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.suzuki)

            } else if (myVehicles?.Model?.contains("nissan", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.nissan)

            } else if (myVehicles?.Model?.contains("mitsubishi", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.mitsubishi)

            } else if (myVehicles?.Model?.contains("mazda", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.mazda)

            } else if (myVehicles?.Model?.contains("mercedes", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.mercedes)

            } else if (myVehicles?.Model?.contains("isuzu", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.isuzu)

            } else if (myVehicles?.Model?.contains("mg", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.mg)

            } else if (myVehicles?.Model?.contains("hyundai", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.hyundai)

            } else if (myVehicles?.Model?.contains("daihatsu", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.daihatsu)

            } else if (myVehicles?.Model?.contains("chevrolet", ignoreCase = true)!!) {
                imgCar.setImageResource(R.drawable.chevrolet)

            } else
            //(myVehicles?.Model?.contains("toyota")!!) {
            {
                imgCar.setImageResource(R.drawable.image_placeholder)

            }


        }


        override fun onClick(v: View?) {
            model?.let { itemClickListener.onItemClick(it, absoluteAdapterPosition, v) }
        }
    }


}

interface ClickEvent {
    fun onDeleteClicked(id: String, position: Int)

}