package com.highbryds.carskoon.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.highbryds.carskoon.R
import com.highbryds.carskoon.interfaces.RVClickCallback
import com.highbryds.carskoon.models.Packages
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.packages_item.view.*


open class PackagesAdapter(
    var context: Context,
    var packages_data: ArrayList<Packages>?,
    var rv: RVClickCallback

)

    : RecyclerView.Adapter<PackagesAdapter.PackagesViewHolder>() {
    var listener: RVClickCallback? = null

    init {
        listener = rv
    }

    fun loadItems(newItems: ArrayList<Packages>, rv: RVClickCallback) {
        packages_data = newItems
        listener = rv
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackagesViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.packages_item, parent, false)

        return PackagesViewHolder(view)
    }

    override fun onBindViewHolder(holder: PackagesViewHolder, position: Int) {

        // Binding all the data with the views.
        try {
            holder.packagetitle?.text = packages_data?.get(position)?.packageName
            holder.pacakgedesc?.text =
                packages_data?.get(position)?.description?.split("Rs")?.get(0)
                    ?.replace("in just", "")
            holder.packageamount?.text = "Rs." +
                    packages_data?.get(position)?.price

            var imageUrl = packages_data?.get(position)?.ImageURL?.lowercase()
            Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.placeholder_img)
                .into(holder.packageimage);


        } catch (e: Exception) {
        }

    }

    override fun getItemCount(): Int {
        var count = 0

        if (packages_data != null && packages_data!!.count() != 0) {
            count = packages_data!!.size
        }
        return count

    }

    inner class PackagesViewHolder
        (itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
        View.OnLongClickListener {


        init {

            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        internal var packagetitle: TextView? = itemView.packagetitle
        internal var pacakgedesc: TextView? = itemView.pacakgedesc
        internal var packageimage: ImageView? = itemView.packageimage
        internal var packageamount: TextView? = itemView.packageamount

        override fun onClick(p0: View?) {
            listener?.onItemClick(adapterPosition)

        }

        override fun onLongClick(p0: View?): Boolean {
            return true

        }


    }


}
