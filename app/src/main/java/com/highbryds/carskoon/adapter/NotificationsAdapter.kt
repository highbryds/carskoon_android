package com.highbryds.carskoon.adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.models.Notifications
import kotlinx.android.synthetic.main.notification_card.view.*


class NotificationsAdapter() :
    BaseRecyclerViewAdapter<Notifications>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.notification_card, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myHolder = holder as? MyViewHolder
        myHolder?.setUpView(getItem(position))
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var model: Notifications? = null


        private val notificationtype: TextView = view.notificationtype
        private val notificationtxt: TextView = view.notification
        private val createdon: TextView = view.createdon
        private val feedback: Button = view.feedback


        fun setUpView(_notification: Notifications?) {
            model = _notification

            notificationtype.text = _notification?.NotificationType
            notificationtxt.text = _notification?.Notification
            createdon.text = _notification?.CreatedOn
            feedback.setOnClickListener(this)
            if (_notification?.IsFeedback == true) {
                feedback.visibility = View.VISIBLE
                feedback.visibility = View.GONE

            } else {
                feedback.visibility = View.GONE


            }


        }


        override fun onClick(v: View?) {


            model?.let { itemClickListener.onItemClick(it, absoluteAdapterPosition, v) }
        }
    }
}