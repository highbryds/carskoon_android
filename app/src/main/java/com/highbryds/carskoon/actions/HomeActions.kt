package com.highbryds.carskoon.actions

/**
 * Sealed Class with static instance that will be used for setting up statuses.
 *
 */

sealed class HomeActions(
) {

    object getpackages : HomeActions()
    object None : HomeActions()
    object packageclick : HomeActions()
//    object Search : LoginActions()

}