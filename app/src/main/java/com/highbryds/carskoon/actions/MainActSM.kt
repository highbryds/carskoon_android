package com.highbryds.carskoon.actions

/**
 * Sealed Class with static instance that will be used for setting up statuses.
 *
 */

sealed class LoginActions(
) {

    object register : LoginActions()
    object None : LoginActions()
//    object Book : LoginActions()
//    object Search : LoginActions()

}