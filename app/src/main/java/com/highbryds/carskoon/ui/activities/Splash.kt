package com.highbryds.carskoon.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.R

class Splash : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // This is used to hide the status bar and make
        // the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        // we used the postDelayed(Runnable, time) method
        // to send a message with a delayed time.
        Handler(Looper.getMainLooper()).postDelayed({
            var intent = Intent(this, Onboarding::class.java)
            if (SharedPreferencesHelper.get(
                    this,
                    preferenceConstants.PREF_APP_LOGIN,
                    false
                ) as Boolean
            ) {
                intent = Intent(this, MainActivity::class.java)


            }
            startActivity(intent)
            finish()
        }, 3000) // 3000 is the delayed time in milliseconds.
    }
}