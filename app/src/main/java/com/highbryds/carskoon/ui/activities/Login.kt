package com.highbryds.carskoon.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import kotlinx.android.synthetic.main.login.*
import java.util.concurrent.TimeUnit

class Login : AppCompatActivity(), View.OnClickListener {
    private lateinit var auth: FirebaseAuth
    var authToken = ""
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        init()

    }

    fun init() {

        auth = Firebase.auth
        submit.setOnClickListener(this)
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {

            }

            override fun onVerificationFailed(p0: FirebaseException) {

            }

            override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(p0, p1)

                authToken = p0

                moveToPinVerification()


            }
        }
    }

    fun moveToPinVerification() {
        GeneralHelper.displayProgressBar(false, progress_bar, this)

        val intent = Intent(this, LoginVerification::class.java)
        intent.putExtra("authToken", authToken)
        intent.putExtra("cellNumber", mobileNumber.text.toString())

        startActivity(intent)

    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.user_tour -> {

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.submit -> {


                var cell_prefix =
                    mobileNumber.text.toString().filter { !it.isWhitespace() }.substring(0, 4)




                if (mobileNumber.text.toString().length < 13) {
                    UIHelper.showLongToastInCenter(
                        this,
                        "Please enter complete cell number in the format +923XXX"
                    )

                    return


                }
                if (cell_prefix.equals("+923")) {

                    GeneralHelper.displayProgressBar(true, progress_bar, this)

                    initiateFirebaseAuth(mobileNumber.text.toString())


                } else {

                    UIHelper.showLongToastInCenter(
                        this,
                        "Please enter cell number in the format +923XXX"
                    )

                    return
                }

            }
        }
    }

    fun initiateFirebaseAuth(cellNumber: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(cellNumber)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }


}