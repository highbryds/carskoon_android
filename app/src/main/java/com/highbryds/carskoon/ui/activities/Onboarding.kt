package com.highbryds.carskoon.ui.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.airbnb.lottie.LottieDrawable
import com.github.appintro.AppIntro
import com.github.appintro.AppIntroFragment
import com.github.appintro.AppIntroPageTransformerType
import com.highbryds.carskoon.R

class Onboarding : AppIntro() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addSliders()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        // Decide what to do when the user clicks on "Skip"
        finish()
        moveToLogin()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        // Decide what to do when the user clicks on "Done"
        finish()
        moveToLogin()
    }

    fun moveToLogin() {

        val intent = Intent(this, Login::class.java)
        startActivity(intent)

    }

    fun addSliders() {

// You can customize your parallax parameters in the constructors.
        setTransformer(
            AppIntroPageTransformerType.Parallax(
                titleParallaxFactor = 1.0,
                imageParallaxFactor = -1.0,
                descriptionParallaxFactor = 2.0
            )
        )
        isColorTransitionsEnabled = true

// Change Indicator Color
        setIndicatorColor(
            selectedIndicatorColor = getColor(R.color.colorPrimaryDark),
            unselectedIndicatorColor = getColor(R.color.colorBlack)
        )
        setBackArrowColor(getColor(R.color.colorPrimaryDark))
        setNextArrowColor(getColor(R.color.colorPrimaryDark))
        setColorSkipButton(getColor(R.color.colorPrimaryDark))
        setColorDoneText(getColor(R.color.colorPrimaryDark))
        addSlide(
            AppIntroFragment.newInstance(
                title = "Hassel Free Car Wash at your Doorstep",
                description = "Washing a car can be more than just wiping off the dirt!\nWash, rinse, shine",
                imageDrawable = R.drawable.onboarding_1,
                // backgroundDrawable = R.drawable.slide_back_1,

                titleColor = Color.BLACK,

                descriptionColor = getColor(R.color.colorPrimaryDark),
                titleTypefaceFontRes = R.font.opensans,
                descriptionTypefaceFontRes = R.font.opensans,

                )
        )
        addSlide(
            AppIntroFragment.newInstance(
                title = "Your car will be sparkling clean with peace of mind",
                description = "We want you clean, we want you fresh, we want your car to shine like new !",
                imageDrawable = R.drawable.onboarding_2,
                titleColor = Color.BLACK,
                descriptionColor = getColor(R.color.colorPrimaryDark),
                
                titleTypefaceFontRes = R.font.opensans,
                descriptionTypefaceFontRes = R.font.opensans,
            )
        )
        addSlide(
            AppIntroFragment.newInstance(
                title = "Enjoy our fast and friendly service",
                description = "The best car wash in your neighborhood, trusted by people who can afford the best !",
                imageDrawable = R.drawable.onboarding_3,
                titleColor = Color.BLACK,
                descriptionColor = getColor(R.color.colorPrimaryDark),
                titleTypefaceFontRes = R.font.opensans,
                descriptionTypefaceFontRes = R.font.opensans,
            )
        )

    }


}