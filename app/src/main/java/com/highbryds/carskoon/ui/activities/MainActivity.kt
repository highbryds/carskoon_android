package com.highbryds.carskoon.ui.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.navigation.NavigationBarView
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Fragments.Home
import com.highbryds.carskoon.Fragments.NotificationsFragment
import com.highbryds.carskoon.Fragments.PaymentHistoryFragment
import com.highbryds.carskoon.Fragments.ProfileTabFragment
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.GeneralHelper.loadFragment
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*


class MainActivity : AppCompatActivity(), NavigationBarView.OnItemSelectedListener,
    TapTargetSequence.Listener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnItemSelectedListener(this);
        loadFragment(Home.newInstance(), this)
        bottomNavigationView.labelVisibilityMode = NavigationBarView.LABEL_VISIBILITY_LABELED
        setSupportActionBar(toolbar)
        if (!(SharedPreferencesHelper.get(
                this,
                preferenceConstants.PREF_TUTORIAL_SHOWN,
                false
            ) as Boolean)
        ) {

            setUpTutorials(
                listOf(


                    TapTarget.forView(
                        findViewById(R.id.miSchedule),
                        "Wash Schedules",
                        "Tap here to Browse your Wash Schedules"
                    ).dimColor(android.R.color.background_light)
                        //.outerCircleColor(R.color.colorPrimaryDark)
                        //.targetCircleColor(R.color.colorPrimary)
                        .textColor(android.R.color.black), TapTarget.forView(
                        findViewById(R.id.miProfile),
                        "Profile",
                        "Tap here to manage vehicles and your wash packages."
                    ).dimColor(android.R.color.background_light)
                        //.outerCircleColor(R.color.colorPrimaryDark)
                        //.targetCircleColor(R.color.colorPrimary)
                        .textColor(android.R.color.black)
                )
            )
        }


        //  var user_data = GeneralHelper.getUserProfileObj(preferenceConstants.PREF_USER_PROFILE)
//        val badge = BadgeDrawable.create(this)
//        BadgeUtils.attachBadgeDrawable(badge, toolbar, R.id.action_notifications)
        fabCall.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:03327937090")
            startActivity(intent)
        }

    }


    @SuppressLint("UnsafeOptInUsageError")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu)
        initBadge(0)

        Handler(Looper.getMainLooper()).post {
            val view: View = findViewById(R.id.action_notifications)
            if (!(SharedPreferencesHelper.get(
                    this,
                    preferenceConstants.PREF_TUTORIAL_SHOWN,
                    false
                ) as Boolean)
            ) {
                setUpTutorials(
                    listOf(

                        TapTarget.forView(
                            view,
                            "Notification",
                            "All app related notifications goes here."
                        ).dimColor(android.R.color.background_light)
                            .textColor(android.R.color.black)
                    )
                )
            }

        }




        return true

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {


                UIHelper.showAlertDialog(
                    "Are you sure you want to logout from the app ?",
                    "Exit App.",
                    this
                )
                    .setPositiveButton(
                        "Yes"
                    ) { dialog, id ->
                        SharedPreferencesHelper.clear(this)

                        finish()
                        val intent = Intent(this, Onboarding::class.java)
                        startActivity(intent)


                    }.setNegativeButton(
                        "No"
                    ) { dialog, id ->
                        dialog.cancel()
                    }.show()


            }
            R.id.action_notifications -> {
                loadFragment(NotificationsFragment.newInstance(), this)

            }
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.miHome -> {
                if (GeneralHelper.getCurrentFragment(this) !is Home) {
                    loadFragment(Home.newInstance(), this)
                }
                true
            }
            R.id.miProfile -> {
                if (GeneralHelper.getCurrentFragment(this) !is ProfileTabFragment) {
                    loadFragment(ProfileTabFragment.newInstance(), this)
                }
                true
            }
            R.id.mipayments -> {

                if (GeneralHelper.getCurrentFragment(this) !is PaymentHistoryFragment) {
                    loadFragment(PaymentHistoryFragment.newInstance(), this)
                }
                true
            }
            R.id.miSchedule -> {
                if (GeneralHelper.getCurrentFragment(this) !is PaymentHistoryFragment) {
                    loadFragment(PaymentHistoryFragment.newInstance(), this)
                }
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {

        if (GeneralHelper.getCurrentFragment(this) is Home) {
            for (i in 1 until supportFragmentManager.backStackEntryCount) {
                supportFragmentManager.popBackStackImmediate()

            }
        }

        var count: Int = supportFragmentManager.backStackEntryCount

        if (count == 1) {

            UIHelper.showAlertDialog("Are you sure you want to exit the app ?", "Exit App.", this)
                .setPositiveButton(
                    "Yes"
                ) { dialog, id ->
                    finish()
                }.setNegativeButton(
                    "No"
                ) { dialog, id ->
                    dialog.cancel()
                }.show()
        }
        //it means you are about to press back in your last activity so do what you must
        else {

            super.onBackPressed()
        }

        if (GeneralHelper.getCurrentFragment(this) is Home) {
            bottomNavigationView.menu.findItem(R.id.miHome).isChecked = true;

        }

    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun initBadge(nbrNotification: Int) {

        if (nbrNotification == 0) return // Don't show the badge

        val badge = BadgeDrawable.create(MainActivity@ this)
        badge.number = nbrNotification

        BadgeUtils.attachBadgeDrawable(
            badge,
            toolbar,
            R.id.action_notifications
        )


    }

    fun setUpTutorials(targetArray: List<TapTarget>) {
        TapTargetSequence(this).targets(targetArray).listener(this).start()


    }

    override fun onSequenceFinish() {
        SharedPreferencesHelper.put(this, preferenceConstants.PREF_TUTORIAL_SHOWN, true)

    }

    override fun onSequenceStep(lastTarget: TapTarget?, targetClicked: Boolean) {
    }

    override fun onSequenceCanceled(lastTarget: TapTarget?) {
    }
}

