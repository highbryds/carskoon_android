package com.highbryds.carskoon.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.gson.Gson
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.models.Table
import com.highbryds.carskoon.viewmodels.LoginViewModel
import com.highbryds.carskoon.viewmodels.LoginViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.user_pin.*
import android.text.Editable

import android.text.TextWatcher
import com.highbryds.carskoon.Helpers.onChange


class LoginVerification : AppCompatActivity(), View.OnClickListener {
    //TODO: variable that needs to be injected.

    private lateinit var loginVM: LoginViewModel
    var authToken = ""
    var cellNumber = ""
    var fcmToken: String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_pin)

        init()


        one.onChange {
            if (it.isNotEmpty()) {
                two.requestFocus()
            }
        }
        two.onChange {
            if (it.isNotEmpty()) {
                three.requestFocus()
            } else if (it.isEmpty()) {
                one.requestFocus()
            }
        }
        three.onChange {
            if (it.isNotEmpty()) {
                four.requestFocus()
            } else if (it.isEmpty()) {
                two.requestFocus()
            }
        }
        four.onChange {
            if (four.text.isNotEmpty()) {
                five.requestFocus()
            } else if (it.isEmpty()) {
                three.requestFocus()
            }
        }
        five.onChange {
            if (it.isNotEmpty()) {
                six.requestFocus()
            } else if (it.isEmpty()) {
                four.requestFocus()
            }
        }
        six.onChange {
            if (it.isEmpty()) {
                five.requestFocus()
            }
        }

    }

    fun init() {
        //TODO: variable that needs to be injected.
        loginVM =
            ViewModelProvider(this, LoginViewModelFactory())
                .get(LoginViewModel::class.java)



        loginVM.getFCMToken()
        toolbar?.title = "Two step verification"
        setSupportActionBar(toolbar)

        continued.setOnClickListener(this)
        authToken = intent.extras!!.getString("authToken", "")
        cellNumber = intent.extras!!.getString("cellNumber", "")
        subscribe()
    }


    fun subscribe() {

        loginVM.getToken().observe(this, {


            fcmToken = it

        })
        loginVM.dataState.observe(this, { dataState ->
// observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<Table>?> -> {
                    dataState.data?.get(0)?.message?.let {
                        UIHelper.showLongToastInCenter(
                            this,
                            it
                        )
                        var user_data = Gson().toJson(dataState.data?.get(0))
                        SharedPreferencesHelper.put(
                            this,
                            preferenceConstants.PREF_USER_PROFILE,
                            user_data
                        )

                      //  GeneralHelper.setliveUserName(dataState.data?.get(0).name)
                        //GeneralHelper.liveUserName.value = dataState.data?.get(0).name
                        moveToMainPage()

                    }
                    GeneralHelper.displayProgressBar(false, progress_bar, this)

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, this)
                    GeneralHelper.displayError(dataState.exception, this)
                }
                is DataState.Loading -> {
                    GeneralHelper.displayProgressBar(true, progress_bar, this)

                }
            }
        })


    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.continued -> {
                if (one.text.toString().equals("") || two.text.toString()
                        .equals("") || three.text.toString().equals("")
                    ||
                    four.text.toString().equals("")
                    ||
                    five.text.toString().equals("")
                    ||
                    six.text.toString().equals("")
                ) {
                    UIHelper.showLongToastInCenter(this, "Incomplete password.")

                    return
                }


                var combinedPin =
                    one.text.toString() + "" + two.text.toString() + "" + three.text.toString() + "" + four.text.toString() + "" + five.text.toString() + "" + six.text.toString() + ""




                verifyCode(combinedPin)


            }
        }
    }

    private fun verifyCode(codeByUser: String) {
        val credential = PhoneAuthProvider.getCredential(authToken, codeByUser)
        signInTheUserByCredentials(credential)
    }

    private fun signInTheUserByCredentials(credential: PhoneAuthCredential) {
        val firebaseAuth = FirebaseAuth.getInstance()
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(this,
                OnCompleteListener<AuthResult?> { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(
                            this,
                            "verification successfull!",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("##res: ", fcmToken!!)
//check if the leading character is + then remove it
                        loginVM.registerCustomer(
                            Table(
                                mobile = cellNumber,
                                whatsapp = cellNumber,
                                fcmtoken = fcmToken!!
                            )
                        )


                    } else {

                        UIHelper.showLongToastInCenter(this, "Error occured")

                    }
                })
    }


    private fun moveToMainPage() {
        SharedPreferencesHelper.put(this, preferenceConstants.PREF_APP_LOGIN, true)
        val intent = Intent(this, MainActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)


    }
}