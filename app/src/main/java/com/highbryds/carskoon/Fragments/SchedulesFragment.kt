package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.SchedulesAdapter
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.models.PaymentHistory
import com.highbryds.carskoon.models.VehicleSchedule
import kotlinx.android.synthetic.main.fragment_my_packages.*
import kotlinx.android.synthetic.main.toolbar.*


class SchedulesFragment : Fragment(),
    BaseRecyclerViewAdapter.OnItemClickListener<PaymentHistory> {
    lateinit var adapterSchedules: SchedulesAdapter
    lateinit var arrayVehicleSchedule: ArrayList<VehicleSchedule>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapterSchedules = context?.let { SchedulesAdapter(it) }!!


    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_packages, container, false)



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (context as AppCompatActivity?)!!.toolbar!!.title = "Wash Schedule"
        initScheduleAdapater(arrayVehicleSchedule)
        noDataFound()
        /*  var mobile = SharedPreferencesHelper.get(
              requireActivity(),
              preferenceConstants.PREF_MOBILE_WITH_ZERO,
              ""
          )*/
        // vmSubscriptionViewModel.getPaymentHistory(mobile.toString())
        // vmSubscriptionViewModel.getPaymentHistory("03452391595")
        // adapterPaymentHistory.setOnItemClickListener(this)
        //   paymentHistory()


    }


    private fun noDataFound() {
        if (adapterSchedules.getList().isEmpty()) {
            imgNoDataFound.visibility = View.VISIBLE
            nodata_txtview.visibility = View.VISIBLE
        } else {
            imgNoDataFound.visibility = View.GONE
            nodata_txtview.visibility = View.GONE

        }
    }

    private fun initScheduleAdapater(arrayList: ArrayList<VehicleSchedule>) {
        val mLayoutManager = LinearLayoutManager(requireContext())
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvMyPackages.layoutManager = mLayoutManager
        rvMyPackages.adapter = adapterSchedules
        adapterSchedules.addItems(arrayList)
        adapterSchedules.notifyDataSetChanged()
    }

    //    private fun paymentHistory() {
//
//
//        vmSubscriptionViewModel.userPaymentHistory.observe(viewLifecycleOwner, { dataState ->
//            // observing data state changes
//
//            when (dataState) {
//
//
//                is DataState.Success<ArrayList<PaymentHistory>?> -> {
//
//                    dataState.data?.let { it1 ->
//
//                        //  UIHelper.showShortToastInCenter(activity, it1.get(0).Status)
//                        initPaymentHistoryAdapater(it1)
//                    }
//
//                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
//
//                }
//                is DataState.Error -> {
//                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
//                    GeneralHelper.displayError(dataState.exception, App.app)
//                }
//                is DataState.Loading -> {
//
//                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
//                }
//            }
//            noDataFound()
//        })
//
//
//    }


    companion object {
        fun newInstance(scheduleArray: ArrayList<VehicleSchedule>): SchedulesFragment {
            val args = Bundle()

            val fragment = SchedulesFragment()
            fragment.arguments = args
            fragment.arrayVehicleSchedule = scheduleArray
            return fragment
        }

    }

    override fun onItemClick(listitem: PaymentHistory, position: Int, view: View?) {
    }

}