package com.highbryds.carskoon.Fragments


import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.MultiAutoCompleteTextView.CommaTokenizer
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.MyVehicleList
import com.highbryds.carskoon.models.Packages
import com.highbryds.carskoon.models.Table
import com.highbryds.carskoon.viewmodels.SharedViewModel
import com.highbryds.carskoon.viewmodels.SubscriptionViewModel
import com.highbryds.carskoon.viewmodels.SubscriptionViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.service_booking_form.*


/**
 * A simple [Fragment] subclass.
 * Use the [SubscribePackageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SubscribePackageFragment : Fragment(), TextWatcher {
    var packageCost = 0
    var current_cars_count: Int = 0
    var mobile = ""
    private lateinit var subscriptionViewModel: SubscriptionViewModel

    var table: Table? = null
    var list_of_items = ""
    private lateinit var adapterUserVehicles: ArrayAdapter<MyVehicleList>
    private var UserVehicles_list: ArrayList<MyVehicleList> = ArrayList()
    private var SelectedVehicles_list: ArrayList<MyVehicleList> = ArrayList()
    private lateinit var objPackages: Packages
    var changeData: MutableLiveData<String> = MutableLiveData()

    private val sharedViewModel: SharedViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("##", "onCreate Called.")

        subscriptionViewModel = ViewModelProvider(
            this,
            SubscriptionViewModelFactory()
        )[SubscriptionViewModel::class.java]


        adapterUserVehicles = ArrayAdapter<MyVehicleList>(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            arrayListOf()
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {


        Log.d("##", "onCreateView Called.")

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.service_booking_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("##", "OnViewCreated Called.")

        mtxtSelectCar.addTextChangedListener(this)
        mtxtSelectCar.setTokenizer(CommaTokenizer())
        clear.setOnClickListener {

            mtxtSelectCar.setText("")

        }

        mobile = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_MOBILE_WITH_ZERO,
            ""
        ) as String
        subscriptionViewModel.getUserVehiclesList(mobile.toString())

        package_selected.setText(objPackages.packageName)
        mtxtSelectCar.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->

                itemVehicleSelected = parent.getItemAtPosition(position) as MyVehicleList


            }

        submit_car.setOnClickListener {

            if (!mtxtSelectCar.text.equals("")) {
                verifySubmittedResult()


            } else {

                UIHelper.showShortToastInCenter(
                    requireActivity(),
                    "please choose vehicle before submitting."
                )

            }

        }
        mtxtSelectCar.setOnClickListener {

            mtxtSelectCar.showDropDown()


        }

//        val closeListener: MyDialogCloseListener = object : MyDialogCloseListener {
//            override fun handleDialogClose(dialog: DialogInterface?) {
//                //do here whatever you want to do on Dialog dismiss
//            }
//        }

//        getCarMakeModel()

//        getUserVehiclesList()

        sharedViewModel.getIsVehicleAdded().observe(requireActivity(), Observer {
            if (it == true) {
                subscriptionViewModel.getUserVehiclesList(mobile.toString())
//                getUserVehiclesList()
            }
        })
    }


    fun verifySubmittedResult() {

        var break_car_array =
            mtxtSelectCar.text.toString().trimEnd().dropLast(1).split(",").distinct()



        SelectedVehicles_list.clear()
        break_car_array.forEachIndexed { index, s ->

            UserVehicles_list.forEachIndexed { _, myVehicleList ->

                val s1 = myVehicleList.Model + "(" + myVehicleList.RegNumber + ")"

                if (break_car_array[index].trimStart().equals(s1.replace(" ", ""))) {
                    SelectedVehicles_list.add(
                        MyVehicleList(
                            V_Id = myVehicleList.Id.toString(),
                            Mobile = myVehicleList.Mobile,
                            Location = myVehicleList.Location,
                            RegNumber = myVehicleList.RegNumber,
                            Lat = myVehicleList.Lat,
                            Lon = myVehicleList.Lon,
                            Package_Id = objPackages.id,
                            Amount = objPackages.price.toInt()
                        )
                    )
                }
            }
        }

        subscriptionViewModel.subscribeToPackage(SelectedVehicles_list)


        subscriptionViewModel.subscribevehicleList.observe(viewLifecycleOwner, { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<MyVehicleList>?> -> {

                    dataState.data?.let { it1 ->

                        Log.d("##", dataState.data.toString())
                        if (it1[0].Status.equals("0")) {
                            UIHelper.showLongToastInCenter(activity, it1[0].Result)

                        } else {
                            GeneralHelper.loadFragment(
                                PaymentFragment.newInstance(it1[0], packageCost),
                                requireActivity()
                            )
                        }


                    }

                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        })

    }


    private lateinit var itemVehicleSelected: MyVehicleList

    //    var isDismiss: Boolean = false
    private fun getUserVehiclesList() {


        subscriptionViewModel.userVehicleList.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {
                is DataState.Success<ArrayList<MyVehicleList>?> -> {

                    dataState.data?.let { it1 ->
                        UserVehicles_list = it1


                        it1.forEachIndexed { _, myVehicleList ->
                            adapterUserVehicles.addAll(myVehicleList)
                        }
                        mtxtSelectCar.setAdapter(adapterUserVehicles)

                        adapterUserVehicles.notifyDataSetChanged()
                        Log.d("##", "outside no data list " + UserVehicles_list.size.toString())


                    }

                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    if (UserVehicles_list.size == 0) {
                        Log.d("##", "inside no data list")

                        val builder = AlertDialog.Builder(activity)
                        builder.setTitle("CarSkoon")
                        builder.setMessage("In Order to subscribe to a package, Please add Vehicles and review them from your profile section of the app !")

                        builder.setPositiveButton("OK") { dialog, which ->
                            dialog.dismiss()

                            AddVehicleFragment_Dialog().show(childFragmentManager, "")


                        }
                        builder.setCancelable(false).show()


                    }
                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        }
//        if(isDismiss){
//            AddVehicleFragment_Dialog().show(childFragmentManager, "")
//        }

    }

    override fun onDestroyView() {
        super.onDestroyView()

    }


    override fun onPause() {
        super.onPause()

    }

    override fun onStop() {
        super.onStop()
        Log.d("super", "onStop")
    }

    override fun onResume() {
        super.onResume()
        Log.d("super", "resume")
        getUserVehiclesList()
//        subscriptionViewModel.getUserVehiclesList(mobile.toString())
    }

    override fun onStart() {
        super.onStart()
        Log.d("super", "onStart")

//        subscriptionViewModel.getIsVehicleAdded().observe(viewLifecycleOwner, Observer {
//            if (it == true) {
//                getUserVehiclesList()
//            }
//        })
    }


    companion object {

        @JvmStatic
        fun newInstance(packages: Packages): SubscribePackageFragment {
            val args = Bundle()

            val fragment = SubscribePackageFragment()
            fragment.arguments = args
            fragment.objPackages = packages
            return fragment
        }

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {

        //unregistering for event in order to prevent infinity loop
        mtxtSelectCar.removeTextChangedListener(this)
        current_cars_count =
            ((s.toString().replace(" ", "").split(",").toTypedArray()).distinct().count()) - 1
        if (current_cars_count == 0) {

            current_cars_count = 1
        }


        var current_car_textField =
            (s.toString().replace(" ", "").split(",").toTypedArray()).distinct()


        packageCost = (current_cars_count * objPackages.price).toInt()
        total_amount.setText(packageCost.toString() + " PKR")
        mtxtSelectCar.setText(current_car_textField.toString().replace("[", "").replace("]", ""))
        mtxtSelectCar.addTextChangedListener(this);
    }

}



