package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.NotificationsAdapter
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.NotificationList
import com.highbryds.carskoon.models.Notifications
import com.highbryds.carskoon.viewmodels.NotificationViewModel
import com.highbryds.carskoon.viewmodels.NotificationViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.fragment_my_notifications.*
import kotlinx.android.synthetic.main.fragment_my_packages.imgNoDataFound
import kotlinx.android.synthetic.main.fragment_my_packages.nodata_txtview
import kotlinx.android.synthetic.main.fragment_my_packages.progress_bar
import kotlinx.android.synthetic.main.toolbar.*


class NotificationsFragment : Fragment(),
    BaseRecyclerViewAdapter.OnItemClickListener<Notifications> {
    lateinit var adapterNotifications: NotificationsAdapter
    private lateinit var arrayNotifications: ArrayList<Notifications>
    private lateinit var notiticationViewModel: NotificationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapterNotifications = NotificationsAdapter()
        arrayNotifications = ArrayList()
        adapterNotifications.setOnItemClickListener(this)


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notiticationViewModel = ViewModelProvider(
            this,
            NotificationViewModelFactory()
        ).get(NotificationViewModel::class.java)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_notifications, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (context as AppCompatActivity?)!!.toolbar!!.title = "My Notifications"
        var mobile = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_MOBILE_WITH_ZERO,
            ""
        )

        notiticationViewModel.getCustomerNotifications(mobile.toString())
        observeDataFromServer()


    }

    fun observeDataFromServer() {

        notiticationViewModel.notificationData.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<NotificationList?> -> {

                    dataState.data?.let { it1 ->
                        arrayNotifications = it1.Notifications
                        initNotificationAdapater(arrayNotifications)


                    }
                    noDataFound()

                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        }

    }


    private fun noDataFound() {
        if (adapterNotifications.getList().isEmpty()) {
            imgNoDataFound.visibility = View.VISIBLE
            nodata_txtview.visibility = View.VISIBLE
        } else {
            imgNoDataFound.visibility = View.GONE
            nodata_txtview.visibility = View.GONE

        }
    }

    private fun initNotificationAdapater(arrayList: ArrayList<Notifications>) {
        val mLayoutManager = LinearLayoutManager(requireContext())
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_notifications.layoutManager = mLayoutManager
        rv_notifications.adapter = adapterNotifications
        adapterNotifications.addItems(arrayList)
        adapterNotifications.notifyDataSetChanged()
    }


    companion object {
        fun newInstance(): NotificationsFragment {
            val args = Bundle()

            val fragment = NotificationsFragment()
            fragment.arguments = args

            return fragment
        }

    }

    override fun onItemClick(listitem: Notifications, position: Int, view: View?) {

        when (view?.id) {
            R.id.feedback -> {
                UIHelper.showShortToastInCenter(activity, "feedback button clicked.")

            }

        }
    }

}