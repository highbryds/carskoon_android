package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.highbryds.carskoon.Constant.AppConstants
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.AutoCompleteAdapter
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.MyVehicleList
import com.highbryds.carskoon.models.MyVehicles
import com.highbryds.carskoon.models.SubmitVehicle
import com.highbryds.carskoon.viewmodels.AddVehicleViewModel
import com.highbryds.carskoon.viewmodels.AddVehicleViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.add_vehicle.*
import kotlinx.android.synthetic.main.toolbar.*


class AddVehicleFragment : Fragment(), View.OnClickListener, UIHelper.getSelectedItemsFromDialog {
    private lateinit var addVehicleVM: AddVehicleViewModel
    var submitCarArray: ArrayList<SubmitVehicle> = ArrayList<SubmitVehicle>()

    var parent: ViewGroup? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        addVehicleVM = ViewModelProvider(
            this,
            AddVehicleViewModelFactory()
        ).get(AddVehicleViewModel::class.java)

        return inflater.inflate(R.layout.add_vehicle, container, false)
    }


    override fun onResume() {
        super.onResume()
        (context as AppCompatActivity?)!!.toolbar!!.title = "Add Vehicle"

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addVehicleVM.getCarMakeModel()
        parent = view.parent as ViewGroup?
        car_color.setOnClickListener(this)
        car_visit_time.setOnClickListener(this)
        car_make_model.setOnClickListener(this)
        submit_car.setOnClickListener(this)
        getCarMakeModel()


    }


    companion object {
        @JvmStatic
        fun newInstance(): AddVehicleFragment {
            val args = Bundle()

            val fragment = AddVehicleFragment()
            fragment.arguments = args
            return fragment
        }

    }


    private fun setupAutoCompleteTextView(carMakeModel: ArrayList<MyVehicles>) {
        // car_make_model.showDropDown()
        val adapter =
            AutoCompleteAdapter(
                requireContext(),
                android.R.layout.simple_list_item_1,
                0,
                carMakeModel
            )
        car_make_model.setAdapter(adapter)
        car_make_model.threshold = 1

    }


    override fun onClick(p0: View?) {
        when (p0?.id) {


            R.id.car_color -> {
                activity?.let {
                    UIHelper.showArrayInDialog(
                        it,
                        AppConstants.getColors(),
                        this,
                        "color"
                    )
                }

            }

            R.id.car_visit_time -> {
                activity?.let {
                    UIHelper.showArrayInDialog(
                        it,
                        AppConstants.getPreferredTimings(),
                        this, "time"
                    )
                }


            }


            R.id.submit_car -> {
                if (car_reg.text!!.equals("") || car_city.text!!.equals("") || car_location.text!!.equals(
                        ""
                    ) || car_make_model.text!!.equals("") || car_color.text!!.equals("") || car_visit_time.text!!.equals(
                        ""
                    )
                ) {

                    UIHelper.showShortToastInCenter(
                        activity,
                        "please fill in complete form to continue."
                    )
                    return
                }
                prepareCarData()
            }
        }

    }

    fun getCarMakeModel() {

        addVehicleVM.vehicle.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<MyVehicles>?> -> {

                    dataState.data?.let { it1 -> setupAutoCompleteTextView(it1) }

                    GeneralHelper.displayProgressBar(false, progress_bar2, requireActivity())


                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar2, requireActivity())


                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar2, requireActivity())


                }
            }

        }


    }

    fun submitCar(cars: ArrayList<SubmitVehicle>) {
        addVehicleVM.submitVehicle(cars)
        activity?.let {
            addVehicleVM.vehicleList.observe(it) { dataState ->
                // observing data state changes

                when (dataState) {


                    is DataState.Success<ArrayList<MyVehicleList>?> -> {

                        dataState.data?.let { it1 ->

                            UIHelper.showShortToastInCenter(activity, it1.get(0).Status)
                            GeneralHelper.popFragment("AddVehicleFragment", requireActivity())
                            GeneralHelper.popFragment("ProfileTabFragment", requireActivity())

                        }

                        GeneralHelper.displayProgressBar(false, progress_bar2, requireActivity())

                    }
                    is DataState.Error -> {
                        GeneralHelper.displayProgressBar(false, progress_bar2, requireActivity())
                        GeneralHelper.displayError(dataState.exception, App.app)
                    }
                    is DataState.Loading -> {

                        GeneralHelper.displayProgressBar(true, progress_bar2, requireActivity())
                    }
                }
            }
        }


    }

    fun prepareCarData() {
        var mobile = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_MOBILE_WITH_ZERO,
            ""
        )


        var mycar = SubmitVehicle(
            mobile.toString(),
            car_make_model.text.toString(),
            car_reg.text.toString(),
            car_color.text.toString(),
            0.0,
            0.0,
            car_location.text.toString(),
            car_visit_time.text.toString()
        )
        submitCarArray.clear()
        submitCarArray.add(mycar)
        submitCar(submitCarArray)


    }

    override fun getColor(color: String) {
        car_color.setText(color)
    }

    override fun getPreferredTimings(timings: String) {
        car_visit_time.setText(timings)
    }



}