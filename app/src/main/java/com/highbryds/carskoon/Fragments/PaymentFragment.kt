package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.MyVehicleList
import com.highbryds.carskoon.models.Payment
import com.highbryds.carskoon.viewmodels.SubscriptionViewModel
import com.highbryds.carskoon.viewmodels.SubscriptionViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.fragment_payment.*


/**
 * A simple [Fragment] subclass.
 * Use the [PaymentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PaymentFragment : Fragment(), View.OnClickListener {
    private lateinit var subscriptionViewModel: SubscriptionViewModel
    var requestType: String = ""
    var transNumber: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        subscriptionViewModel = ViewModelProvider(
            this,
            SubscriptionViewModelFactory()
        )[SubscriptionViewModel::class.java]

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.highbryds.carskoon.R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var paymentModes = arrayOf("Payment Method", "CASH", "Bank Transfer")
        val paymentModesAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item, paymentModes
        )

        paymentModesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner1.setAdapter(paymentModesAdapter)
        spinner1.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>?, arg1: View, arg2: Int, arg3: Long) {
                val items: String = spinner1.getSelectedItem().toString()
                requestType = items
                if (requestType.equals("CASH")) {
                    hideAccountDetails()
                } else {
                    showFields()

                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>?) {}
        })

        txtAmount.text = "Amount: Rs. " + packageCost + ""



        if (myVehicleList.Amount.toString().equals("0")) {

            hideFields()

        } else {
            showFields()

        }


        subscriptionViewModel.dataState.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {
                is DataState.Success -> {

                    dataState.data?.let { it ->
                        GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())


                        if (it.Table?.get(0)?.Status == 1) {

                            var msg = ""


                            if (requestType.equals("CASH")) {
                                msg = "Our rider will contact you for cash collection."


                            } else if (requestType.equals("Bank Transfer")) {
                                msg = "Please wait while we verify your payment."


                            } else if (myVehicleList.Amount == 0) {
                                msg = "Our team will contact your soon."


                            }

                            UIHelper.showAlertDialog(msg, "Subscribe package", requireActivity())
                                .setPositiveButton("OK") { dialog, which ->
                                    dialog.dismiss()
                                    GeneralHelper.popStackUntil(1, requireActivity())


                                }.setCancelable(false).show()


                        }

                    }


                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }

            }

        }
        submit.setOnClickListener(this)


    }

    lateinit var myVehicleList: MyVehicleList;
    var packageCost: Int = 0;

    companion object {

        @JvmStatic
        fun newInstance(myVehicleList: MyVehicleList, packageCost: Int): PaymentFragment {
            val args = Bundle()

            val fragment = PaymentFragment()
            fragment.arguments = args
            fragment.myVehicleList = myVehicleList
            fragment.packageCost = packageCost

            return fragment
        }

    }

    override fun onClick(v: View?) {

        if (requestType.equals("") && myVehicleList.Amount != 0) {
            UIHelper.showShortToastInCenter(requireActivity(), "Please choose transaction type.")
            return
        }
        if (requestType.equals("Payment Method")) {
            UIHelper.showShortToastInCenter(
                requireActivity(),
                "Please choose any payment method to continue."
            )
            return
        }

        if (requestType.equals("Bank Transfer") && txtTransactionNo.text.toString().equals("")) {
            UIHelper.showShortToastInCenter(
                requireActivity(),
                "Transaction number cannot be left blank."
            )
            return
        }
        if (myVehicleList.Amount == 0) {
            requestType == "CASH"

        }

        if (requestType.equals("CASH")) {
            transNumber = "CASH"

        } else {

            transNumber = txtTransactionNo.text.toString()
        }
        val payment = Payment(
            OrderNo = myVehicleList.OrderNo,
            MobileNo = myVehicleList.Mobile,
            PackageId = myVehicleList.Package_Id.toString(),
            RequestType = requestType,
            Description = txtBankDetails.text.toString(),
            Amount = packageCost,

            TrnxNo = transNumber,

            )

        subscriptionViewModel.initiatePaymentRequest(payment)
    }


    fun hideFields() {
        spinner1.visibility = View.GONE
        hideAccountDetails()
    }

    fun hideAccountDetails() {

        acc.visibility = View.GONE
        acc_title.visibility = View.GONE
        bank.visibility = View.GONE
        txtAmount.visibility = View.GONE
        txtTransactionNo.visibility = View.GONE
    }

    fun showFields() {

        spinner1.visibility = View.VISIBLE
        acc.visibility = View.VISIBLE
        acc_title.visibility = View.VISIBLE
        bank.visibility = View.VISIBLE
        txtAmount.visibility = View.VISIBLE
        txtTransactionNo.visibility = View.VISIBLE


    }
}