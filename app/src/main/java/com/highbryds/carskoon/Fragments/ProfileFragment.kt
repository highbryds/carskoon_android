package com.highbryds.carskoon.Fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.highbryds.carskoon.Constant.AppConstants
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.models.Table
import com.highbryds.carskoon.viewmodels.ProfileViewModel
import com.highbryds.carskoon.viewmodels.ProfileViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.fragment_profile.*


/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {


    lateinit var profileViewModel: ProfileViewModel
    lateinit var userProfileData: Table

    var table: Table? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileViewModel = ViewModelProvider(
            this,
            ProfileViewModelFactory()
        ).get(ProfileViewModel::class.java)


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            AppConstants.getCities()
        )
        spCities.adapter = adapter

        //value initialization
        setInit()

        proceed.setOnClickListener {
            table = Table(
                mobile = txtMobile.text.toString(),
                whatsapp = txtWhatsAppNo.text.toString(),
                area = txtAddress.text.toString(),
                city = spCities.selectedItem.toString(),
                fcmtoken = userProfileData.fcmtoken,
                name = txtName.text.toString()
            )
            profileViewModel.updateCustomerProfile(table!!)
        }


        profileViewModel.dataState.observe(requireActivity(), Observer {

            when (it) {

                is DataState.Success<ArrayList<Table>?> -> {

                    it.data?.let { it1 ->

                        GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                        if (it.data[0].status.equals("1")) {


                            if (table != null) {
                                SharedPreferencesHelper.put(
                                    requireActivity(),
                                    preferenceConstants.PREF_USER_PROFILE,
                                    Gson().toJson(table)
                                )

                                UIHelper.showShortToastInCenter(
                                    requireActivity(),
                                    it.data[0].message!!
                                )


                            }

                        } else {

                            UIHelper.showShortToastInCenter(
                                requireActivity(),
                                "Error occured while saving the data."
                            )


                        }

                    }


                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }

            }
        })


    }

    override fun onDestroyView() {
        super.onDestroyView()

    }

    fun setInit() {
        var userData = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_USER_PROFILE,
            ""
        )
        if (userData != null && userData != "") {
            userProfileData = Gson().fromJson(userData.toString(), Table::class.java)

            txtName.setText(userProfileData.name)
            txtMobile.setText(userProfileData.mobile)
            txtWhatsAppNo.setText(userProfileData.whatsapp)
            txtAddress.setText(userProfileData.area)
            spCities.setSelection(AppConstants.getCities().indexOf(userProfileData.city))
        }


    }

    override fun onPause() {
        super.onPause()

    }

    override fun onStop() {
        super.onStop()
    }

    companion object {

        @JvmStatic
        fun newInstance(): ProfileFragment {
            val args = Bundle()

            val fragment = ProfileFragment()
            fragment.arguments = args
            return fragment
        }

    }
}