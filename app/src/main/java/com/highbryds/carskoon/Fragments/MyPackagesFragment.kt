package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.MyPackagesAdapter
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.Packages
import com.highbryds.carskoon.viewmodels.SubscriptionViewModel
import com.highbryds.carskoon.viewmodels.SubscriptionViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.fragment_my_packages.*
import kotlinx.android.synthetic.main.fragment_my_packages.imgNoDataFound
import kotlinx.android.synthetic.main.fragment_my_packages.nodata_txtview
import kotlinx.android.synthetic.main.fragment_my_vehicle.*
import kotlinx.android.synthetic.main.fragment_my_vehicle.progress_bar
import kotlinx.android.synthetic.main.toolbar.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MyPackagesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MyPackagesFragment : Fragment()/*, BaseRecyclerViewAdapter.OnItemClickListener<Packages>*/ {
    lateinit var adapterMyPackages: MyPackagesAdapter
    private lateinit var vmSubscriptionViewModel: SubscriptionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapterMyPackages = context?.let { MyPackagesAdapter(it) }!!

        vmSubscriptionViewModel = ViewModelProvider(
            this,
            SubscriptionViewModelFactory()
        ).get(SubscriptionViewModel::class.java)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_packages, container, false)
        (activity as AppCompatActivity?)!!.toolbar!!.title = "My Packages"

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var mobile = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_MOBILE_WITH_ZERO,
            ""
        )
        vmSubscriptionViewModel.getUserPackagesList(mobile.toString())
        initMyPackages()
        myPackageslist()

    }

    private fun noDataFound() {


        if (adapterMyPackages.getList().isEmpty()) {
            imgNoDataFound.visibility = View.VISIBLE
            nodata_txtview.visibility = View.VISIBLE
        } else {
            imgNoDataFound.visibility = View.GONE
            nodata_txtview.visibility = View.GONE

        }
    }

    fun notifyOnChange(arrayList: ArrayList<Packages>) {

        adapterMyPackages.clear()
        adapterMyPackages.addItems(arrayList)
        adapterMyPackages.notifyDataSetChanged()


    }


    private fun initMyPackages() {
        val mLayoutManager = LinearLayoutManager(requireContext())
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvMyPackages.layoutManager = mLayoutManager
        rvMyPackages.adapter = adapterMyPackages


    }

    fun myPackageslist() {


        vmSubscriptionViewModel.userPackagesList.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<Packages>?> -> {

                    dataState.data?.let { it1 ->
                        notifyOnChange(it1)


                    }
                    noDataFound()
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)

                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        }


    }


    companion object {
        fun newInstance(): MyPackagesFragment {
            val args = Bundle()

            val fragment = MyPackagesFragment()
            fragment.arguments = args
            return fragment
        }

    }


}