package com.highbryds.carskoon.Fragments

//import com.highbryds.carskoon.adapter.ProfileTabAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.ProfileTabAdapter

import kotlinx.android.synthetic.main.fragment_profile_tab.*
import kotlinx.android.synthetic.main.toolbar.*


/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileTabFragment : Fragment() {


    override fun onStart() {
        super.onStart()
        (context as AppCompatActivity?)!!.toolbar!!.title = "Personal Prefences"

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_tab, container, false)

    }

    var myAdapter: ProfileTabAdapter? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL


        myAdapter = ProfileTabAdapter(childFragmentManager, lifecycle)

        viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL

        viewPager.adapter = myAdapter

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->

            when (position) {
                0 -> { tab.text = "Profile"
                }
                1 -> { tab.text = "My Vehicles"
                }
                2 -> { tab.text = "My Packages"
                }
            }

        }.attach()



        myAdapter!!.notifyDataSetChanged()


    }

    companion object {

        fun newInstance(): ProfileTabFragment {
            val args = Bundle()

            val fragment = ProfileTabFragment()
            fragment.arguments = args
            return fragment
        }


    }

    override fun onStop() {
        super.onStop()
    }

    override fun onPause() {
        super.onPause()

    }


}