package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.PaymentHistoryAdapter
import com.highbryds.carskoon.adapter.abstracts.BaseRecyclerViewAdapter
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.PaymentHistory
import com.highbryds.carskoon.models.VehicleSchedule
import com.highbryds.carskoon.viewmodels.SubscriptionViewModel
import com.highbryds.carskoon.viewmodels.SubscriptionViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.fragment_my_packages.*
import kotlinx.android.synthetic.main.toolbar.*


class PaymentHistoryFragment : Fragment(),
    BaseRecyclerViewAdapter.OnItemClickListener<PaymentHistory> {
    lateinit var adapterPaymentHistory: PaymentHistoryAdapter
    private lateinit var vmSubscriptionViewModel: SubscriptionViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapterPaymentHistory = context?.let { PaymentHistoryAdapter(it) }!!

        vmSubscriptionViewModel = ViewModelProvider(
            this,
            SubscriptionViewModelFactory()
        ).get(SubscriptionViewModel::class.java)


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_packages, container, false)

//        (context as AppCompatActivity?)!!.toolbar!!.title = "Payment History"

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (context as AppCompatActivity?)!!.toolbar!!.title = "Payment & Schedules"

        var mobile = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_MOBILE_WITH_ZERO,
            ""
        )
        // vmSubscriptionViewModel.getPaymentHistory(mobile.toString())
        vmSubscriptionViewModel.getPaymentHistory(mobile.toString())
        adapterPaymentHistory.setOnItemClickListener(this)
        SubscribeToVehicleSchedule()

        SubscribepaymentHistory()


    }


    private fun noDataFound() {
        if (adapterPaymentHistory.getList().isEmpty()) {
            imgNoDataFound.visibility = View.VISIBLE
            nodata_txtview.visibility = View.VISIBLE
        } else {
            imgNoDataFound.visibility = View.GONE
            nodata_txtview.visibility = View.GONE

        }
    }

    private fun initPaymentHistoryAdapater(arrayList: ArrayList<PaymentHistory>) {
        val mLayoutManager = LinearLayoutManager(requireContext())
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvMyPackages.layoutManager = mLayoutManager
        rvMyPackages.adapter = adapterPaymentHistory
        adapterPaymentHistory.clear()
        adapterPaymentHistory.addItems(arrayList)
    }

    private fun SubscribepaymentHistory() {


        vmSubscriptionViewModel.userPaymentHistory.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<PaymentHistory>?> -> {

                    dataState.data?.let { it1 ->

                        //  UIHelper.showShortToastInCenter(activity, it1.get(0).Status)
                        initPaymentHistoryAdapater(it1)
                    }

                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {
                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
            noDataFound()

        }


    }

    fun SubscribeToVehicleSchedule() {
        vmSubscriptionViewModel.vehicleSchedule.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<VehicleSchedule>?> -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    dataState.data?.let { it1 ->
// data available move to next fragment with the data.

                        if (dataState.data.size == 0 || dataState.data == null) {

                            UIHelper.showShortToastInCenter(
                                activity,
                                "No Schedule available for the selected vehicle. ."
                            )

                        } else {

                            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {

                                // open fragment with data.
                                GeneralHelper.loadFragment(
                                    SchedulesFragment.newInstance(dataState.data),
                                    requireActivity()


                                )
                            }


                        }


                    }


                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        }


    }

    companion object {
        fun newInstance(): PaymentHistoryFragment {
            val args = Bundle()

            val fragment = PaymentHistoryFragment()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onItemClick(listitem: PaymentHistory, position: Int, view: View?) {
        vmSubscriptionViewModel.getSchedules(listitem.orderNo)


    }

}