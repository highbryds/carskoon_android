package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.PackagesAdapter
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.interfaces.RVClickCallback
import com.highbryds.carskoon.models.Packages
import com.highbryds.carskoon.models.Table
import com.highbryds.carskoon.viewmodels.HomeFragmentModelFactory
import com.highbryds.carskoon.viewmodels.HomeFragmentViewModel
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.home.*
import kotlinx.android.synthetic.main.toolbar.*


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class Home : Fragment(), RVClickCallback {


    private lateinit var HomeVM: HomeFragmentViewModel
    private lateinit var packagesAdapter: PackagesAdapter
    var packages_data: ArrayList<Packages>? = ArrayList()


    /*
      setupApptList will be used to setup appartment list
       */
    fun setupPackagesList(packages: ArrayList<Packages>) {

        rv_packages.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        // Create a DividerItemDecoration whose orientation is vertical
        val vItemDecoration = DividerItemDecoration(
            activity,
            DividerItemDecoration.VERTICAL
        )

        // add decoration for list
        rv_packages.addItemDecoration(
            vItemDecoration
        )
        // setting up recyclerview and also binding activity with the view-model
        packagesAdapter = PackagesAdapter(
            activity?.applicationContext!!,
            packages_data,
            this
        )
        rv_packages?.adapter = packagesAdapter


    }

    fun subscribe() {
        var Username = GeneralHelper.getUserProfileObj(
            SharedPreferencesHelper.get(
                requireActivity(),
                preferenceConstants.PREF_USER_PROFILE,
                ""
            ).toString()
        ).name
        if (Username != null) {
            welcome.text =
                "Welcome " + Username

        }

        activity?.let {
            HomeVM.dataState.observe(it) { dataState ->
                // observing data state changes

                when (dataState) {


                    is DataState.Success<ArrayList<Packages>?> -> {

                        dataState.data?.let { it1 -> packagesAdapter.loadItems(it1, this) }
                        packagesAdapter.notifyDataSetChanged()
                        GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                    }
                    is DataState.Error -> {
                        GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                        GeneralHelper.displayError(dataState.exception, App.app)
                    }
                    is DataState.Loading -> {

                        GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                    }
                }
            }
        }


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        HomeVM = ViewModelProvider(
            this,
            HomeFragmentModelFactory()
        ).get(HomeFragmentViewModel::class.java)

        val view = inflater.inflate(R.layout.home, container, false)
        return view

    }

    lateinit var userProfileData: Table
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        packages_data?.let { setupPackagesList(it) }
        subscribe()

        (activity as AppCompatActivity?)!!.toolbar!!.title = "Wash Packages"


        var userData = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_USER_PROFILE,
            ""
        )
        if (userData != null && userData != "") {
            userProfileData = Gson().fromJson(userData.toString(), Table::class.java)

            var mobileNumber = userProfileData.mobile!!
            mobileNumber = mobileNumber.replace("+92", "0")

            SharedPreferencesHelper.put(
                requireContext(),
                preferenceConstants.PREF_MOBILE_WITH_ZERO,
                mobileNumber
            )


        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onItemClick(pos: Int) {

        GeneralHelper.loadFragment(
            SubscribePackageFragment.newInstance(
                packagesAdapter.packages_data?.get(
                    pos
                )!!
            ), requireActivity()
        )

    }

    companion object {
        @JvmStatic
        fun newInstance(): Home {
            val args = Bundle()

            val fragment = Home()
            fragment.arguments = args
            return fragment
        }
    }
}