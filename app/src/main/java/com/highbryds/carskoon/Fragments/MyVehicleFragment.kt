package com.highbryds.carskoon.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.highbryds.carskoon.Constant.preferenceConstants
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.Helpers.SharedPreferencesHelper
import com.highbryds.carskoon.Helpers.UIHelper
import com.highbryds.carskoon.R
import com.highbryds.carskoon.adapter.ClickEvent
import com.highbryds.carskoon.adapter.MyVehicleAdapter
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.MyVehicleList
import com.highbryds.carskoon.models.deleteVehicle
import com.highbryds.carskoon.viewmodels.AddVehicleViewModel
import com.highbryds.carskoon.viewmodels.AddVehicleViewModelFactory
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.android.synthetic.main.fragment_my_vehicle.*
import kotlinx.android.synthetic.main.toolbar.*


class MyVehicleFragment : Fragment(), ClickEvent {
    lateinit var adapterMyVehicle: MyVehicleAdapter
    private lateinit var addVehicleVM: AddVehicleViewModel
    var position_to_delete = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapterMyVehicle = context?.let { MyVehicleAdapter(this) }!!


        addVehicleVM = ViewModelProvider(
            this,
            AddVehicleViewModelFactory()
        ).get(AddVehicleViewModel::class.java)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_vehicle, container, false)
        (activity as AppCompatActivity?)!!.toolbar!!.title = "My Vehicles"

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var mobile = SharedPreferencesHelper.get(
            requireActivity(),
            preferenceConstants.PREF_MOBILE_WITH_ZERO,
            ""
        )

        addVehicleVM.getVehicleProfile(mobile.toString())

        initMyVehicleList()

        subscribeToVehicleChange()

        add_vehicle.setOnClickListener { view ->


            GeneralHelper.loadFragment(AddVehicleFragment.newInstance(), requireActivity())
        }
    }

    private fun noDataFound() {
        if (adapterMyVehicle.getList().isEmpty()) {
            imgNoDataFound.visibility = View.VISIBLE
            nodata_txtview.visibility = View.VISIBLE
        } else {
            imgNoDataFound.visibility = View.GONE
            nodata_txtview.visibility = View.GONE

        }
    }


    fun notifyOnChange(arrayList: ArrayList<MyVehicleList>) {

        adapterMyVehicle.clear()
        adapterMyVehicle.addItems(arrayList)
        adapterMyVehicle.notifyDataSetChanged()


    }

    private fun initMyVehicleList() {
        val mLayoutManager = LinearLayoutManager(requireContext())
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvMyVehicle.layoutManager = mLayoutManager
        rvMyVehicle.adapter = adapterMyVehicle


    }

    fun subscribeToVehicleChange() {


        addVehicleVM.vehicleList.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<MyVehicleList>?> -> {

                    dataState.data?.let { it1 ->
                        notifyOnChange(it1)


                    }
                    noDataFound()
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        }

        //setting up observer for deleting action.
        addVehicleVM.deleteRes.observe(viewLifecycleOwner) { dataState ->
            // observing data state changes

            when (dataState) {


                is DataState.Success<ArrayList<deleteVehicle>?> -> {

                    dataState.data?.let { it1 ->

                        adapterMyVehicle.remove(position_to_delete)
                        it1[0].Result?.let {
                            UIHelper.showShortToastInCenter(
                                requireActivity(),
                                it
                            )
                            noDataFound()

                        }
                    }
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())

                }
                is DataState.Error -> {
                    GeneralHelper.displayProgressBar(false, progress_bar, requireActivity())
                    GeneralHelper.displayError(dataState.exception, App.app)
                }
                is DataState.Loading -> {

                    GeneralHelper.displayProgressBar(true, progress_bar, requireActivity())
                }
            }
        }


    }


    companion object {
        fun newInstance(): MyVehicleFragment {
            val args = Bundle()

            val fragment = MyVehicleFragment()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onDeleteClicked(id: String, position: Int) {
        UIHelper.showAlertDialog(
            "Are you sure you want to delete this car ?",
            "Delete Car",
            context
        ).setPositiveButton("YES") { dialog, which ->
            dialog.dismiss()
            position_to_delete = position
            addVehicleVM.deleteVehicle(id)


        }
            .setNegativeButton("No") { dialog, which ->
                dialog.dismiss()

            }.show()

    }


}