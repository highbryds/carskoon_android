package com.highbryds.carskoon.Helpers

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.tasks.Tasks
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.highbryds.carskoon.R
import com.highbryds.carskoon.models.Table
import java.util.concurrent.ExecutionException


object GeneralHelper {


    @JvmStatic
    fun getToken(): String? {
        val task = FirebaseMessaging.getInstance().token
        try {
            val result = Tasks.await(task)
            return result
        } catch (e: ExecutionException) {
            throw IllegalArgumentException("")
        } catch (e: InterruptedException) {
            throw IllegalArgumentException("")
        }
    }

    /**
     * Display progressbar
     */
    fun displayProgressBar(isDisplayed: Boolean, progess: View, context: Activity) {

        if (isDisplayed) {

            context.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            );
        } else {
            context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


        }

        progess.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    /**
     * This method will be used to show the error messages if any
     */
    fun displayError(message: String?, context: Context) {

        var msg: String?
        msg = message
        if (message == null) {

            msg = "unknown error"
        }
        UIHelper.showShortToastInCenter(context, msg!!)


    }

    fun getCurrentFragment(activity: Activity): Fragment? {
        return (activity as FragmentActivity).supportFragmentManager
            .findFragmentById(R.id.fragment_container)
    }

    fun loadFragment(fragment: Fragment, activity: Activity) {

        val fragmentManager = (activity as FragmentActivity).supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(
            R.id.fragment_container,
            fragment,
            fragment.javaClass.simpleName
        ).addToBackStack(null).commit()
    }

    fun loadFragment_onBackStack(fragment: Fragment, activity: Activity) {

        val fragmentManager = (activity as FragmentActivity).supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(
            R.id.fragment_container,
            fragment,
            fragment.javaClass.simpleName
        ).addToBackStack("subscribe").commit()
    }

/*    fun loadFragment2(fragment: Fragment, activity: Activity, v: View) {
        val fragmentManager = (activity as FragmentActivity).supportFragmentManager
//getChildFragmentManager()
        //  val fragment: Fragment = NewFragment()
        fragmentManager.beginTransaction()
            .replace(R.id.frame, fragment)
            .addToBackStack(null)
            .commit()
//        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.replace(
//            R.id.fragment_container,
//            fragment,
//            fragment.javaClass.simpleName
//        )
//        fragmentTransaction.addToBackStack(null)
//
//            // fragmentTransaction.addToBackStack(if (fragmentManager.backStackEntryCount == 0) fragment.javaClass.simpleName else null)
//            .commit()
    }*/

    fun getUserProfileObj(userData: String): Table {

        return Gson().fromJson(userData.toString(), Table::class.java)


    }

    fun popStackUntil(count: Int, activity: Activity) {
        for (i in count until (activity as FragmentActivity).supportFragmentManager.getBackStackEntryCount()) {
            (activity as FragmentActivity).supportFragmentManager.popBackStackImmediate()

        }

    }

    fun popFragment(tag: String, context: Activity) {
        var isPopped =
            (context as FragmentActivity).supportFragmentManager.popBackStackImmediate(
                tag,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
        if (!isPopped) {
            (context as FragmentActivity).supportFragmentManager.popBackStack()
            //maybe a loop until you reached your goal.
        }
    }

    private fun emptyBackStack(context: Activity) {
        val fm: FragmentManager = (context as FragmentActivity).supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
    }
//    fun dateFormate(inputDate: String):String{
////        val oldPattern: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
////        val newPattern: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd")
////        val datetime: LocalDateTime = LocalDateTime.parse(inputDate, oldPattern)
////
////        return datetime.format(newPattern)
//    }


}
