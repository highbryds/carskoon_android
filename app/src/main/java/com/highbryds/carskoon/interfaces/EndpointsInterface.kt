package com.highbryds.carskoon.interfaces


import com.highbryds.carskoon.models.*
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EndpointsInterface {

    @POST("RegisterCustomer")
    suspend fun registerCustomer(
        @Body
        customer: Table

    ): Response<ArrayList<Table>>

    @GET("getpackages")
    suspend fun getPackages(


    ): Response<ArrayList<Packages>>

    @POST("UpdateCustomer")
    suspend fun UpdateCustomer(
        @Body
        customer: Table

    ): Response<ArrayList<Table>>


    @GET("getVehicle")
    suspend fun getVehicle(


    ): Response<ArrayList<MyVehicles>>


    @POST("InsertVehicleProfile")
    suspend fun InsertVehicleProfile(
        @Body
        vehicleList: ArrayList<SubmitVehicle>

    ): Response<ArrayList<MyVehicleList>>

    @GET("GetVehicleProfile/{cellnumber}")
    suspend fun GetVehicleProfile(

        @Path("cellnumber") cellnumber: String,
    ): Response<ArrayList<MyVehicleList>>


    @GET("GetMyVehicleForPackages/{cellnumber}")
    suspend fun getUserVehicles(

        @Path("cellnumber") cellnumber: String,
    ): Response<ArrayList<MyVehicleList>>


    @POST("SubscribePackage")
    suspend fun subscribePackage(
        @Body
        vehicleList: ArrayList<MyVehicleList>

    ): Response<ArrayList<MyVehicleList>>

    @GET("MyActiveSubscriptions/{cellnumber}")
    suspend fun getUserPackages(

        @Path("cellnumber") cellnumber: String,
    ): Response<ArrayList<Packages>>


    @POST("InitiatePaymentRequest")
    suspend fun initiatePaymentRequest(
        @Body payment: Payment,
    ): Response<TableData>


    @GET("MyPaymentRequestSummary/{cellnumber}")
    suspend fun paymentRequestSummary(
        @Path("cellnumber") cellnumber: String,
    ): Response<ArrayList<PaymentHistory>>

    @GET("removeVehicle/{vehicleId}")
    suspend fun removeVehicle(
        @Path("vehicleId") vehicleId: String,
    ): Response<ArrayList<deleteVehicle>>

    @GET("GetVehicleScheduleByOrder/{orderId}")
    suspend fun GetVehicleScheduleByOrder(
        @Path("orderId") orderId: String,
    ): Response<ArrayList<VehicleSchedule>>

// get all notifications from cellnumber.
    @POST("GetCustomerNotificationListing")
    suspend fun GetCustomerNotificationListing(
    @Body
    MobileNo: RequestBody,
    ): Response<NotificationList>
}



