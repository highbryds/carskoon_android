package com.highbryds.carskoon.interfaces

/**
 * Interface RVClickCallback contains functions for list callbacks.
 */
interface RVClickCallback {
    fun onItemClick(pos: Int)

}