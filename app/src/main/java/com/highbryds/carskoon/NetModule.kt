package com.highbryds.carskoon

import com.highbryds.carskoon.interfaces.EndpointsInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//class NetModule// Constructor needs one parameter to instantiate.
//(internal var mBaseUrl: String) {

object NetModule// Constructor needs one parameter to instantiate.
// (internal var mBaseUrl: String) {
{
    val httpLoggingInterceptor: HttpLoggingInterceptor
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {

                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            } else {

                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC

            }
            return httpLoggingInterceptor
        }


    fun getApiInterface(retroFit: Retrofit): EndpointsInterface {
        return retroFit.create(EndpointsInterface::class.java)
    }

    fun getRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.carskoon.com/api/home/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    fun _getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.carskoon.com/api/home/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpCleint(httpLoggingInterceptor))
            .build()
    }

    fun getOkHttpCleint(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }


}