package com.highbryds.carskoon.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




//class TableResponse {
//    private var Status: Int? = null
//
//    @SerializedName("Result")
//    @Expose
//    private var result: String? = null
//
//    fun getStatus(): Int? {
//        return Status
//    }
//
//    fun setStatus(status: Int?) {
//        this.Status = status
//    }
//
//    fun getResult(): String? {
//        return result
//    }
//
//    fun setResult(result: String?) {
//        this.result = result
//    }
//}


data class TableResponse(


    @SerializedName("Status") val Status: Int? = null,

    @SerializedName("Result") val Result: String = ""

)