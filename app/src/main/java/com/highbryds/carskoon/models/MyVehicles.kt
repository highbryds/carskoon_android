package com.highbryds.carskoon.models


data class MyVehicles(
    val id: Int,
    val Make: String,
    val Model: String,
    val regNo: String,
    val color: String,

    ) {
    override fun toString(): String {
        return "$Make ($Model)"
    }
}
