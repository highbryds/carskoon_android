package com.highbryds.carskoon.models


data class MyVehicleList(

    var Id: Int =0,
    var V_Id: String="",
    var Make: String="",
    var Model: String="",
    var Mobile: String="",
    var RegNumber: String="",
    var Color: String="",
    var Lat: String?="",
    var Lon: String?="",
    var Location: String="",
    var PreferredTimming: String="",
    var Status: String="",
    var Result: String="",
    var IsDeleted: String="",
    var OrderNo: String ="",


//adding these keys for purchased subscription package
    var Address: String="",
    var Package_Id: Int=0,
    var Amount: Int=0,


    ){

    override fun toString(): String {
        return "$Model($RegNumber)"
    }


}
