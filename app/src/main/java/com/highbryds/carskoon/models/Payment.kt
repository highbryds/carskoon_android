package com.highbryds.carskoon.models

import com.google.gson.annotations.SerializedName


data class Payment(

   var OrderNo: String,
   var MobileNo:String,
   var PackageId :String,
   var RequestType :String,
   var Description :String,
   var Amount :Int,
   var TrnxNo :String = "",


)