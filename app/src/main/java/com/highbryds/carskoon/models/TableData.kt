package com.highbryds.carskoon.models
import com.google.gson.annotations.SerializedName


data class TableData(


    @SerializedName("Table") val Table: ArrayList<TableResponse>? = null

)