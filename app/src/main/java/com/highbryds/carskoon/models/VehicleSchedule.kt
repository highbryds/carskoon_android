package com.highbryds.carskoon.models


data class VehicleSchedule(

    val RegNumber: String,
    val WashingTime: String,
    val WashingDate: String,

    )