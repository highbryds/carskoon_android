package com.highbryds.carskoon.models


data class SubmitVehicle(

    val Mobile: String,
    val Model: String,
    val RegNumber: String,
    val Color: String,
    val Lat: Double,
    val Lon: Double,
    val Location: String,
    val PreferredTimming: String,

    )