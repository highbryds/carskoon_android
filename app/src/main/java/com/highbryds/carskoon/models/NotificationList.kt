package com.highbryds.carskoon.models

import com.google.gson.annotations.SerializedName


data class NotificationList (

  @SerializedName("Table" ) var Notifications : ArrayList<Notifications> = arrayListOf()

)

