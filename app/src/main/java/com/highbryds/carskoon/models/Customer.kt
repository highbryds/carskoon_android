package com.highbryds.carskoon.models

data class Customer(

    var Name: String? = null,
    var Nic: String? = null,
    var Mobile: String,
    var Whatsapp: String,
    var Address: String? = null,
    var Email: String? = null,
    var Landmark: String? = null,
    var Lat: String? = null,
    var Lon: String? = null,
    var Location: String? = null,
    var Fcmtoken: String,
    var Area: String? = null,
    var Preferedtimming: String? = null,
    var Type: String? = null,
    var City: String? = null,
    var RegisterationSource: Int = 1,
    var mode: String = "R",

    )