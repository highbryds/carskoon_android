package com.highbryds.carskoon.models

import com.google.gson.annotations.SerializedName

data class Table(


    @SerializedName("status") val status: String? = null,
    @SerializedName("message") val message: String? = null,
    @SerializedName("Id") val id: Int? = null,
    @SerializedName("Name") val name: String? = null,
    @SerializedName("AccountCode") val accountCode: String? = null,
    @SerializedName("Nic") val nic: String? = null,
    @SerializedName("Mobile") val mobile: String? = null,
    @SerializedName("Whatsapp") val whatsapp: String? = null,
    @SerializedName("Address") val address: String? = null,
    @SerializedName("Email") val email: String? = null,
    @SerializedName("Landmark") val landmark: String? = null,
    @SerializedName("Lat") val lat: String? = null,
    @SerializedName("Lon") val lon: String? = null,
    @SerializedName("Location") val location: String? = null,
    @SerializedName("Fcmtoken") val fcmtoken: String,
    @SerializedName("Area") val area: String? = null,
    @SerializedName("Addon") val addon: String? = null,
    @SerializedName("AddBy") val addBy: String? = null,
    @SerializedName("Preferedtimming") val preferedtimming: String? = null,
    @SerializedName("IsActive") val isActive: String? = null,
    @SerializedName("IsDeleted") val isDeleted: String? = null,
    @SerializedName("Type") val type: String? = null,
    @SerializedName("City") val city: String? = null,
    @SerializedName("RegisterationSource") val registerationSource: Int = 1,
    @SerializedName("Mode") val mode: String = "R"

)