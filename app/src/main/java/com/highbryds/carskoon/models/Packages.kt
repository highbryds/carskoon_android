package com.highbryds.carskoon.models

import com.google.gson.annotations.SerializedName


data class Packages(

    @SerializedName("id") val id: Int,
    @SerializedName("PackageName") val packageName: String,
    @SerializedName("PackageCode") val packageCode: String,
    @SerializedName("PackageType") val packageType: String,
    @SerializedName("VehicleTypeId") val vehicleTypeId: String,
    @SerializedName("Price") val price: Double,
    @SerializedName("TotalDays") val totalDays: Int,
    @SerializedName("Description") val description: String,
    @SerializedName("isActive") val isActive: Boolean,
    @SerializedName("AddOn") val addOn: String,
    @SerializedName("AddBy") val addBy: String,
    @SerializedName("ImageURL") val ImageURL: String,



    //my subscribed packages
    @SerializedName("Mobile") val Mobile: String,
    @SerializedName("VehicleProfile_Id") val VehicleProfileId: String,
    @SerializedName("TotalAmount") val TotalAmount: String,
    @SerializedName("PaymentStatus") val PaymentStatus: String,
    @SerializedName("TotalDue") val TotalDue: String,
    @SerializedName("StartDate") val StartDate: String,
    @SerializedName("EndDate") val EndDate: String="",
    @SerializedName("Package") val Package: String,
    @SerializedName("RegNumber") val RegNumber: String,
)