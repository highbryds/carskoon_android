package com.highbryds.carskoon.models

import com.google.gson.annotations.SerializedName


data class Notifications (

  @SerializedName("LogID"            ) var LogID            : Int?     = null,
  @SerializedName("JobId"            ) var JobId            : String?  = null,
  @SerializedName("NotificationType" ) var NotificationType : String?  = null,
  @SerializedName("Notification"     ) var Notification     : String?  = null,
  @SerializedName("IsFeedback"       ) var IsFeedback       : Boolean? = null,
  @SerializedName("CreatedOn"        ) var CreatedOn        : String?  = null

)