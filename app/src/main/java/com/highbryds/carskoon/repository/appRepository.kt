package com.highbryds.carskoon.repository


import android.location.Location
import com.highbryds.carskoon.NetModule
import com.highbryds.carskoon.interfaces.EndpointsInterface
import com.highbryds.carskoon.models.*
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Retrofit


//03328211595
//class appRepository {
object appRepository {

    //TODO: variables that needs to be injected.
   // var c = context
    var retrofit: Retrofit
 //   var fusedLocationClient: FusedLocationProviderClient


    init {
     //   fusedLocationClient = LocationServices.getFusedLocationProviderClient(c)
        retrofit = NetModule._getRetrofit()

    }
//
//    @SuppressLint("MissingPermission")
//    // we acquired location already
//    fun getmyLocation(ILocation: locationSuccess) {
//
//        fusedLocationClient.lastLocation
//            .addOnSuccessListener { location: Location? ->
//                // Got last known location. In some rare situations this can be null.
//                if (location != null) {
//// callback for location
//                    ILocation.getLocation(location)
//
//                }
//
//            }
//
//
//    }

    // function to register/login customer.
     fun registerCustomer(
        customer: Table
    ): Flow<DataState<ArrayList<Table>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.registerCustomer(
                        Table(
                            mobile = customer.mobile,
                            whatsapp = customer.whatsapp,
                            fcmtoken = customer.fcmtoken
                        )
                    )


                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

    // function for getting the packages.
     fun getPackages(

    ): Flow<DataState<ArrayList<Packages>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.getPackages(

                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }
// get car make and model....


     fun getCarMakeModel(

    ): Flow<DataState<ArrayList<MyVehicles>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.getVehicle(

                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }
// submit vehicle

     fun submitVehicle(
        vehicleList: ArrayList<SubmitVehicle>

    ): Flow<DataState<ArrayList<MyVehicleList>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.InsertVehicleProfile(
                        vehicleList
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

// get all the vehicles on a mobile number...

     fun getVehiclesFromMobileNumber(
        cellNumber: String
    ): Flow<DataState<ArrayList<MyVehicleList>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.GetVehicleProfile(
                        cellNumber
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

    // function for getting the packages.
    fun updateProfile(table: Table): Flow<DataState<ArrayList<Table>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.UpdateCustomer(table)



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api = apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

    //get User vehicles
     fun getUserVehiclesMobileNumber(
        cellNumber: String
    ): Flow<DataState<ArrayList<MyVehicleList>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.getUserVehicles(
                        cellNumber
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

    //user subscribe package
     fun subscribePackage(
        vehicleList: ArrayList<MyVehicleList>

    ): Flow<DataState<ArrayList<MyVehicleList>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.subscribePackage(
                        vehicleList
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }


    //user subscribed packages

     fun getUserPackages(
        cellNumber: String
    ): Flow<DataState<ArrayList<Packages>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.getUserPackages(
                        cellNumber
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }




    //user initiatePaymentRequest
     fun initiatePaymentRequest(
        payment: Payment

    ): Flow<DataState<TableData?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.initiatePaymentRequest(
                        payment
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }




    //user user payment history

     fun getPaymentHistory(
        cellNumber: String
    ): Flow<DataState<ArrayList<PaymentHistory>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.paymentRequestSummary(
                        cellNumber
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

    //delete vehicle from id
     fun deleteVehicleFromId(
        Id: String
    ): Flow<DataState<ArrayList<deleteVehicle>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.removeVehicle(
                        Id
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

    // get schedules from order number.

    suspend fun getSchedules(
        orderNumber: String
    ): Flow<DataState<ArrayList<VehicleSchedule>?>> =

        flow {


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.GetVehicleScheduleByOrder(
                        orderNumber
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }


    // get all customer notifications from cell number.
    fun getAllCustomerNotifications(
        cellNumber: String
    ): Flow<DataState<NotificationList?>> =

        flow {


            val jsonObject = JSONObject()
            jsonObject.put("MobileNo", cellNumber)

            val body = jsonObject.toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())


            val endpoints = retrofit.create(EndpointsInterface::class.java)

            emit(DataState.Loading)

            try {

                var apiResponse =
                    endpoints.GetCustomerNotificationListing(
                        body
                    )



                when (apiResponse.code()) {
                    200 -> {

                        // first of getting the result into an array

                        var res_from_api =
                            apiResponse.body()


                        emit(DataState.Success(res_from_api))
                    }
                    400 -> {
                        emit(DataState.Error("400 - Bad request"))

                    }
                    404 -> {
                        emit(DataState.Error("404 - Not found"))

                    }
                    500 -> {
                        emit(DataState.Error("500 - Internal server error"))

                    }
                    403 -> {
                        emit(DataState.Error("500 - Forbidden"))

                    }
                    401 -> {
                        emit(DataState.Error("401 -  Unauthorized"))

                    }
                    else -> {
                        emit(DataState.Error("Error occured with response code " + apiResponse.code()))
                    }

                }


            } catch (e: Exception) {
                emit(DataState.Error(e.message.toString()))
            }

        }

}

interface locationSuccess {

    fun getLocation(loc: Location)

}



