package com.highbryds.carskoon.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.highbryds.carskoon.Helpers.GeneralHelper
import com.highbryds.carskoon.actions.LoginActions
import com.highbryds.carskoon.models.Table
import com.highbryds.carskoon.repository.appRepository
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel : ViewModel() {
    //TODO: variable that needs to be injected.

   // var dbrepo: appRepository
    private val _dataState: MutableLiveData<DataState<ArrayList<Table>?>> = MutableLiveData()
    private val _fcmToken: MutableLiveData<String> = MutableLiveData()

    val dataState: MutableLiveData<DataState<ArrayList<Table>?>>
        get() = _dataState

    val fcmToken: MutableLiveData<String>
        get() = fcmToken

    init {

    }

    fun registerCustomer(customer: Table?) {
        setStateEvent(LoginActions.register, customer)


    }

     fun getToken(): MutableLiveData<String> {

        return _fcmToken
    }

    fun getFCMToken() {
        // var result: String? = null
        viewModelScope.launch {
            _fcmToken.value = withContext(Dispatchers.IO) {
                GeneralHelper.getToken()
            }!!

            // val result = GeneralHelper.getToken()
            // Log.e("result", result.toString())

            // viewModelScope.launch {
            // _fcmToken.value = GeneralHelper.getToken()
            //  }


        }
        //return result
    }

    /**
     * Method for setting up state
     * @param mainStateEvent - instance of sealed  class - MainActSM.kt file.
     */
    fun setStateEvent(mainStateEvent: LoginActions, customer: Table?) {

        viewModelScope.launch {
            when (mainStateEvent) {
                is LoginActions.register -> {
                    //  getting appartment's data
                    appRepository.registerCustomer(customer!!)
                        .onEach { dataState ->
                            _dataState.value = dataState
                        }
                        .launchIn(viewModelScope)
                }

                LoginActions.None -> {
                    // For now nothing...
                }
          }
        }
    }

}