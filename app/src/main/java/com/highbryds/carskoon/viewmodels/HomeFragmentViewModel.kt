package com.highbryds.carskoon.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.highbryds.carskoon.actions.HomeActions
import com.highbryds.carskoon.app.App
import com.highbryds.carskoon.models.Packages
import com.highbryds.carskoon.repository.appRepository
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class HomeFragmentViewModel : ViewModel() {
    //var dbrepo: appRepository
    private val _dataState: MutableLiveData<DataState<ArrayList<Packages>?>> = MutableLiveData()

    val dataState: MutableLiveData<DataState<ArrayList<Packages>?>>
        get() = _dataState

    init {

        getPackages()

    }

    fun getPackages() {
        setStateEvent(HomeActions.getpackages)


    }

    /**
     * Method for setting up state
     * @param HomeActions - instance of sealed  class - HomeActions.kt file.
     */
    fun setStateEvent(homeActions: HomeActions) {

        viewModelScope.launch {
            when (homeActions) {
                is HomeActions.getpackages -> {
                    //  getting package's data
                    appRepository.getPackages()
                        .onEach { dataState ->
                            _dataState.value = dataState
                        }
                        .launchIn(viewModelScope)
                }

                HomeActions.None -> {
                    // For now nothing...
                }
                HomeActions.packageclick -> {
                    // For now nothing...
                }
            }
        }
    }

}