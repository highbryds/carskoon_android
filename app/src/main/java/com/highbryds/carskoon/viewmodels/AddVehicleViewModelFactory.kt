package com.highbryds.carskoon.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AddVehicleViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(AddVehicleViewModel::class.java)) {

            AddVehicleViewModel() as T

        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }


}

