package com.highbryds.carskoon.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ProfileViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {

            ProfileViewModel() as T

        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }


}

