package com.highbryds.carskoon.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.highbryds.carskoon.models.NotificationList
import com.highbryds.carskoon.repository.appRepository
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class NotificationViewModel : ViewModel() {

    private val _notificationData: MutableLiveData<DataState<NotificationList?>> =
        MutableLiveData()
    val notificationData: MutableLiveData<DataState<NotificationList?>>
        get() = _notificationData


    init {
        Log.d("##", "Add NotificationViewModel called.")

    }

    fun getCustomerNotifications(cellNumber: String) {
        viewModelScope.launch {
            appRepository.getAllCustomerNotifications(cellNumber).collect { result ->
                _notificationData.value = result
            }


        }

    }


}
