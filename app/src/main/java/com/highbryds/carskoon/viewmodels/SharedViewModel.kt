package com.highbryds.carskoon.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {

    private var isVehicleAdded = MutableLiveData<Boolean>()
    fun setIsVehicleAdded(input: Boolean) {
        isVehicleAdded.value = input
    }

    fun getIsVehicleAdded(): LiveData<Boolean> {
        return isVehicleAdded
    }
}