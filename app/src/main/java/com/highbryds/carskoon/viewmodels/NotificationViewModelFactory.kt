package com.highbryds.carskoon.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NotificationViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {

            NotificationViewModel() as T

        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }


}

