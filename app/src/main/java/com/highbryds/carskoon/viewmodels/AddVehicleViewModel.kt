package com.highbryds.carskoon.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.highbryds.carskoon.models.MyVehicleList
import com.highbryds.carskoon.models.MyVehicles
import com.highbryds.carskoon.models.SubmitVehicle
import com.highbryds.carskoon.models.deleteVehicle
import com.highbryds.carskoon.repository.appRepository
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


class AddVehicleViewModel : ViewModel() {
    //TODO: variable that needs to be injected.

   // var dbrepo: appRepository
    private val _vehicleList: MutableLiveData<DataState<ArrayList<MyVehicleList>?>> =
        MutableLiveData()
    val vehicleList: MutableLiveData<DataState<ArrayList<MyVehicleList>?>>
        get() = _vehicleList


    private val _vehicle: MutableLiveData<DataState<ArrayList<MyVehicles>?>> = MutableLiveData()
    val vehicle: MutableLiveData<DataState<ArrayList<MyVehicles>?>>
        get() = _vehicle


    private val _deleteRes: MutableLiveData<DataState<ArrayList<deleteVehicle>?>> =
        MutableLiveData()
    val deleteRes: MutableLiveData<DataState<ArrayList<deleteVehicle>?>>
        get() = _deleteRes


    init {

    }

    fun getCarMakeModel() {
        viewModelScope.launch {

            appRepository.getCarMakeModel()
                .onEach { vehicle ->
                    _vehicle.value = vehicle
                }.collect()
        }

    }


    //  fun submitVehicle list
    fun submitVehicle(cars: ArrayList<SubmitVehicle>) {
        viewModelScope.launch {

            appRepository.submitVehicle(cars)
                .onEach { vehicles ->
                    _vehicleList.value = vehicles
                }.collect()
        }

    }

    //get vehicles from cellnumber.
    fun getVehicleProfile(cell: String) {
        viewModelScope.launch {

            appRepository.getVehiclesFromMobileNumber(cell)
                .onEach { vehicles ->
                    _vehicleList.value = vehicles
                }.collect()
        }


    }

    //deleting vehicle.

    fun deleteVehicle(id: String) {
        viewModelScope.launch {

            appRepository.deleteVehicleFromId(id)
                .onEach { result ->
                    _deleteRes.value = result
                }.collect()
        }


    }


}