package com.highbryds.carskoon.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.highbryds.carskoon.models.*
import com.highbryds.carskoon.repository.appRepository
import com.highbryds.carskoon.viewstate.DataState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


class SubscriptionViewModel : ViewModel() {
    //TODO: variable that needs to be injected.

   // var dbrepo: appRepository
    private val _userVehicleList: MutableLiveData<DataState<ArrayList<MyVehicleList>?>> =
        MutableLiveData()
    val userVehicleList: MutableLiveData<DataState<ArrayList<MyVehicleList>?>> get() = _userVehicleList

    private val _subscribevehicleList: MutableLiveData<DataState<ArrayList<MyVehicleList>?>> =
        MutableLiveData()
    val subscribevehicleList: MutableLiveData<DataState<ArrayList<MyVehicleList>?>> get() = _subscribevehicleList

    private val _userPackagesList: MutableLiveData<DataState<ArrayList<Packages>?>> =
        MutableLiveData()
    val userPackagesList: MutableLiveData<DataState<ArrayList<Packages>?>> get() = _userPackagesList

    private val _userPaymentHistory: MutableLiveData<DataState<ArrayList<PaymentHistory>?>> =
        MutableLiveData()
    val userPaymentHistory: MutableLiveData<DataState<ArrayList<PaymentHistory>?>> get() = _userPaymentHistory


    private val _vehicleSchedule: MutableLiveData<DataState<ArrayList<VehicleSchedule>?>> =
        MutableLiveData()
    val vehicleSchedule: MutableLiveData<DataState<ArrayList<VehicleSchedule>?>> get() = _vehicleSchedule


    private val _dataState: MutableLiveData<DataState<TableData?>> = MutableLiveData()

    val dataState: MutableLiveData<DataState<TableData?>>
        get() = _dataState




    init {

    }

    fun getUserVehiclesList(mobileNumber: String) {
        viewModelScope.launch {

            appRepository.getUserVehiclesMobileNumber(mobileNumber)
                .onEach { vehicle ->
                    _userVehicleList.value = vehicle
                }.collect()
        }

    }

    fun subscribeToPackage(vehicleList: ArrayList<MyVehicleList>) {
        viewModelScope.launch {

            appRepository.subscribePackage(vehicleList)
                .onEach { vehicle ->
                    _subscribevehicleList.value = vehicle
                }.collect()
        }

    }


    fun getUserPackagesList(mobileNumber: String) {
        viewModelScope.launch {

            appRepository.getUserPackages(mobileNumber)
                .onEach { packages ->
                    _userPackagesList.value = packages
                }.collect()
        }

    }

    fun initiatePaymentRequest(payment: Payment) {
        viewModelScope.launch {

            appRepository.initiatePaymentRequest(payment)
                .onEach { dataState ->

                    _dataState.value = dataState
                }.collect()
        }

    }

    fun getPaymentHistory(mobileNumber: String) {
        Log.d("##", "called getpaymentHistory")
        viewModelScope.launch {

            appRepository.getPaymentHistory(mobileNumber)
                .onEach { paymentHistory ->
                    userPaymentHistory.value = paymentHistory
                }.collect()
        }

    }

    fun getSchedules(orderNumber: String) {
        viewModelScope.launch {

            appRepository.getSchedules(orderNumber)
                .onEach { schedules ->
                    vehicleSchedule.value = schedules
                }.collect()
        }

    }

}