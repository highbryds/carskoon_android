package com.highbryds.carskoon.Constant

import com.highbryds.carskoon.models.MyVehicles

class AppConstants {
    companion object {


        fun getCities(): ArrayList<String> {
            val arrCities: ArrayList<String> = ArrayList()
            arrCities.add("Karachi")
            // arrCities.add("Lahore")
            // arrCities.add("Islamabad")
            return arrCities

        }

        fun getColors(): ArrayList<String> {
            val arrColors: ArrayList<String> = ArrayList()
            arrColors.add("White")
            arrColors.add("Black")
            arrColors.add("Red")
            arrColors.add("Silver")
            arrColors.add("Grey")
            arrColors.add("Maroon")
            arrColors.add("Blue")
            arrColors.add("Green")
            arrColors.add("Brown")
            arrColors.add("Red Wine")
            arrColors.add("Sky Blue")

            return arrColors

        }

        fun getPreferredTimings(): ArrayList<String> {
            val arrtimings: ArrayList<String> = ArrayList()
            arrtimings.add("12AM - 3AM")
            arrtimings.add("3AM - 6AM")
            arrtimings.add("6AM - 9AM")
            arrtimings.add("6AM - 12PM")
            arrtimings.add("3PM - 6AM")
            arrtimings.add("6PM - 9PM")
            arrtimings.add("9PM - 12AM")



            return arrtimings

        }

        fun getMyVehicleList(): ArrayList<MyVehicles> {
            val arrMyVehicles: ArrayList<MyVehicles> = ArrayList()
            arrMyVehicles.add(
                MyVehicles(
                    1,
                    "Suzuki",
                    "Swift",
                    "Rgs234",
                    "Blue"
                )
            )
            arrMyVehicles.add(
                MyVehicles(
                    1,
                    "Suzuki",
                    "Swift",
                    "Rgs234",
                    "Blue"
                )
            )
            arrMyVehicles.add(
                MyVehicles(
                    1,
                    "Suzuki",
                    "Swift",
                    "Rgs234",
                    "Blue"
                )
            )

            return arrMyVehicles

        }
    }
}