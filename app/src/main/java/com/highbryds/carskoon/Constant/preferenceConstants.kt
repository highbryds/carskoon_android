package com.highbryds.carskoon.Constant


object preferenceConstants {
    const val PREF_USER_PROFILE = "user_profile"
    const val PREF_APP_LOGIN = "app_login"
    const val PREF_MOBILE_WITH_ZERO = "mobile_num_with_zero"
    const val PREF_TUTORIAL_SHOWN = "tutorial_shown"

}